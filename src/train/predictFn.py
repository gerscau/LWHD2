import tensorflow as tf
from tool import common
import matplotlib.pyplot as plt
import numpy as np

def predict(predictor, x):
    x0 = tf.compat.v1.train.Example(features=tf.train.Features(feature={"x": common._float_feature(value=x)}))
    x1 = x0.SerializeToString()
    y = predictor({"receiver_tensors": [x1]})
    return y


def predict_tic_type():
    print('reading...')
    (x, y) = common.readData_csv(common.data_folder + common.dataProcess_tic_type.testName_csv)
    print('reading done')

    with tf.compat.v1.Session(graph=tf.compat.v1.Graph()):
        predictor = tf.compat.v1.contrib.predictor.from_saved_model('modelPython')

        rows = len(y)
        splitIndex = 100
        indices = np.random.permutation(rows)
        trainIndex = indices[:splitIndex]

        imageRow = 6
        imageCol = 12

        for i in trainIndex:
            output = predict(predictor, x[i])
            r1 = output['r'][0]
            r1 = r1.reshape(224, 224)
            temp = output['m1'][0]
            temp = temp.transpose(2, 0, 1)
            plt.subplot(imageRow, imageCol, 1)
            plt.imshow(r1, cmap='gray', origin='lower')

            c = imageRow * imageCol - 1
            if temp.shape[0] < c:
                c = temp.shape[0]
            for j in range(c):
                plt.subplot(imageRow, imageCol, j + 2)
                plt.imshow(temp[j], cmap='gray', origin='lower')
            plt.show()
