import tensorflow as tf
from tensorflow.python.ops import metrics_impl


def calc_relative_std(labels, predictions):
    with tf.compat.v1.name_scope('relative_std'):
        error = tf.compat.v1.div((predictions - labels), labels)
        meanError = tf.reduce_mean(error)
        square = tf.square(error - meanError)
        mean_tensor, mean_update_op = metrics_impl.mean(square)
        sqrt_tensor = metrics_impl.math_ops.sqrt(mean_tensor)
        sqrt_update_op = metrics_impl.math_ops.sqrt(mean_update_op)
        return sqrt_tensor, sqrt_update_op


def calc_std(labels, predictions):
    with tf.compat.v1.name_scope('std'):
        error = predictions - labels
        meanError = tf.reduce_mean(error)
        square = tf.square(error - meanError)
        mean_tensor, mean_update_op = metrics_impl.mean(square)
        sqrt_tensor = metrics_impl.math_ops.sqrt(mean_tensor)
        sqrt_update_op = metrics_impl.math_ops.sqrt(mean_update_op)
        return sqrt_tensor, sqrt_update_op


def regression_vgg64_fn(features, labels, mode, params):

    x = tf.compat.v1.feature_column.input_layer(features, params["feature_columns"])
    x = tf.identity(x, 'input')
    r1 = tf.reshape(x, shape=[-1, 64, 64, 1])
# ========================================================================
    c11 = tf.compat.v1.layers.conv2d(r1, 64, 3, activation=tf.nn.relu, padding='same')
    c12 = tf.compat.v1.layers.conv2d(c11, 64, 3, activation=tf.nn.relu, padding='same')
    c13 = tf.compat.v1.layers.conv2d(c12, 64, 3, activation=tf.nn.relu, padding='same')
    m1 = tf.compat.v1.layers.average_pooling2d(c13, pool_size=3, strides=2, padding='same')

    c21 = tf.compat.v1.layers.conv2d(m1, 128, 3, activation=tf.nn.relu, padding='same')
    c22 = tf.compat.v1.layers.conv2d(c21, 128, 3, activation=tf.nn.relu, padding='same')
    c23 = tf.compat.v1.layers.conv2d(c22, 128, 3, activation=tf.nn.relu, padding='same')
    m2 = tf.compat.v1.layers.average_pooling2d(c23, pool_size=3, strides=2, padding='same')

    c31 = tf.compat.v1.layers.conv2d(m2, 256, 3, activation=tf.nn.relu, padding='same')
    c32 = tf.compat.v1.layers.conv2d(c31, 256, 3, activation=tf.nn.relu, padding='same')
    c33 = tf.compat.v1.layers.conv2d(c32, 256, 3, activation=tf.nn.relu, padding='same')
    c34 = tf.compat.v1.layers.conv2d(c33, 256, 3, activation=tf.nn.relu, padding='same')
    m3 = tf.compat.v1.layers.average_pooling2d(c34, pool_size=3, strides=2, padding='same')

    c41 = tf.compat.v1.layers.conv2d(m3, 512, 3, activation=tf.nn.relu, padding='same')
    c42 = tf.compat.v1.layers.conv2d(c41, 512, 3, activation=tf.nn.relu, padding='same')
    c43 = tf.compat.v1.layers.conv2d(c42, 512, 3, activation=tf.nn.relu, padding='same')
    c44 = tf.compat.v1.layers.conv2d(c43, 512, 3, activation=tf.nn.relu, padding='same')
    m4 = tf.compat.v1.layers.average_pooling2d(c44, pool_size=2, strides=2)

    # ===================================================================
    f1 = tf.compat.v1.layers.flatten(m4)
    d1 = tf.compat.v1.layers.dense(f1, 1024, activation=tf.nn.relu)
    d2 = tf.compat.v1.layers.dense(d1, 256, activation=tf.nn.relu)
    d3 = tf.compat.v1.layers.dense(d2, units=1)
    y = tf.squeeze(d3, 1, name='output')
# PREDICT==================================================
    if (mode == tf.compat.v1.estimator.ModeKeys.PREDICT):
        predictions = {'y': y, 'r1': r1, 'm1': m1, 'm2': m2, 'm3': m3, 'm4': m4}
#         predictions = {'y': y, 'r1': r1, 'c11': c11, 'c12': c12}
        export_outputs = {'predOutput': tf.compat.v1.estimator.export.PredictOutput(predictions)}
        return tf.compat.v1.estimator.EstimatorSpec(mode=mode, predictions=predictions, export_outputs=export_outputs)
# TRAIN==================================================
    labels = tf.squeeze(labels['y'], 1)
    average_loss = tf.compat.v1.losses.absolute_difference(labels, y, weights=1)

    if (mode == tf.compat.v1.estimator.ModeKeys.TRAIN):
        optimizer = tf.compat.v1.train.AdamOptimizer(learning_rate=params.get("learning_rate"), epsilon=params.get("epsilon"))
        train_op = optimizer.minimize(loss=average_loss, global_step=tf.compat.v1.train.get_global_step())
        return tf.compat.v1.estimator.EstimatorSpec(mode=mode, loss=average_loss, train_op=train_op)

# EVAL==================================================
    std = calc_std(labels, y)
    eval_metrics = {"std": std}
    return tf.compat.v1.estimator.EstimatorSpec(mode=mode, loss=average_loss, eval_metric_ops=eval_metrics)


def regression_vgg48_fn(features, labels, mode, params):
#     if (mode == tf.estimator.ModeKeys.TRAIN):
#         is_training = True
#     else:
#         is_training = False

    x = tf.compat.v1.feature_column.input_layer(features, params["feature_columns"])
    x = tf.identity(x, 'input')
    x = tf.reshape(x, shape=[-1, 48, 48, 1])
    r = x
    # ========================================================================
    x = tf.compat.v1.layers.conv2d(r, 64, 3, activation=tf.nn.relu, padding='same')
    x = tf.compat.v1.layers.average_pooling2d(x, pool_size=2, strides=2, padding='same')
    m1 = x

    x = tf.compat.v1.layers.conv2d(x, 128, 3, activation=tf.nn.relu, padding='same')
    x = tf.compat.v1.layers.average_pooling2d(x, pool_size=2, strides=2, padding='same')
    m2 = x

    x = tf.compat.v1.layers.conv2d(x, 256, 3, activation=tf.nn.relu, padding='same')
    x = tf.compat.v1.layers.conv2d(x, 256, 3, activation=tf.nn.relu, padding='same')
    x = tf.compat.v1.layers.average_pooling2d(x, pool_size=2, strides=2, padding='same')
    m3 = x
    # ===================================================================
    x = tf.compat.v1.layers.flatten(x)
    x = tf.compat.v1.layers.dense(x, 512, activation=tf.nn.relu)
    x = tf.compat.v1.layers.dense(x, 512, activation=tf.nn.relu)
    x = tf.compat.v1.layers.dense(x, units=1)
    y = tf.squeeze(x, 1, name='output')
# PREDICT==================================================
    if (mode == tf.compat.v1.estimator.ModeKeys.PREDICT):
        predictions = {'y': y, 'r': r, 'm1': m1, 'm2': m2, 'm3': m3}
        export_outputs = {'predOutput': tf.estimator.export.PredictOutput(predictions)}
        return tf.compat.v1.estimator.EstimatorSpec(mode=mode, predictions=predictions, export_outputs=export_outputs)
# TRAIN==================================================
    labels = tf.squeeze(labels['y'], 1)
    average_loss = tf.compat.v1.losses.absolute_difference(labels, y, weights=1)

    if (mode == tf.compat.v1.estimator.ModeKeys.TRAIN):
        optimizer = tf.compat.v1.train.AdamOptimizer(learning_rate=params.get("learning_rate"), epsilon=params.get("epsilon"))
        train_op = optimizer.minimize(loss=average_loss, global_step=tf.compat.v1.train.get_global_step())
        return tf.compat.v1.estimator.EstimatorSpec(mode=mode, loss=average_loss, train_op=train_op)

# EVAL==================================================
    std = calc_std(labels, y)
    eval_metrics = {"std": std}
    return tf.compat.v1.estimator.EstimatorSpec(mode=mode, loss=average_loss, eval_metric_ops=eval_metrics)


def classification_vgg48_fn(features, labels, mode, params):

    if (mode == tf.estimator.ModeKeys.TRAIN):
        is_training = True
    else:
        is_training = False

    x = tf.compat.v1.feature_column.input_layer(features, params["feature_columns"])
    x = tf.compat.v1.identity(x, name='input')
    # 数组新的shape属性应该要与原来的配套，如果等于-1的话，那么Numpy会根据剩下的维度计算出数组的另外一个shape属性值
    x = tf.compat.v1.reshape(x, shape=[-1, 48, 48, 1])
    r = x
# ========================================================================
    x = tf.compat.v1.layers.conv2d(x, 64, 5, activation=tf.nn.relu, padding='same')
    x = tf.compat.v1.layers.conv2d(x, 64, 5, activation=tf.nn.relu, padding='same')
    x = tf.compat.v1.layers.max_pooling2d(x, pool_size=2, strides=2, padding='same')
    m1 = x
    x = tf.compat.v1.layers.conv2d(x, 128, 5, activation=tf.nn.relu, padding='same')
    x = tf.compat.v1.layers.max_pooling2d(x, pool_size=2, strides=2, padding='same')
    m2 = x
    x = tf.compat.v1.layers.conv2d(x, 256, 3, activation=tf.nn.relu, padding='same')
    x = tf.compat.v1.layers.conv2d(x, 64, 1, activation=tf.nn.relu, padding='same')
    x = tf.compat.v1.layers.max_pooling2d(x, pool_size=2, strides=2, padding='same')
    m3 = x
    # ===================================================================
    rate = tf.compat.v1.Variable(0.5, name='rate')
    x = tf.compat.v1.layers.flatten(x)
    x = tf.compat.v1.layers.dense(x, 512, activation=tf.nn.relu)
    x = tf.compat.v1.layers.dropout(x, rate=rate, training=is_training)
    x = tf.compat.v1.layers.dense(x, 256, activation=tf.nn.relu)
    x = tf.compat.v1.layers.dropout(x, rate=rate, training=is_training)
    logits = tf.compat.v1.layers.dense(x, params["ySize"])
    tf.compat.v1.identity(logits, name='output')
    predicted_classes = tf.compat.v1.argmax(input=logits, axis=1)
# PREDICT==================================================
    if (mode == tf.estimator.ModeKeys.PREDICT):
        predictions = {'logits': logits, 'r': r, 'm1': m1, 'm2': m2, 'm3': m3}
        export_outputs = {'predOutput': tf.estimator.export.PredictOutput(predictions)}
        return tf.compat.v1.estimator.EstimatorSpec(mode=mode, predictions=predictions, export_outputs=export_outputs)
# TRAIN==================================================
    labels = tf.compat.v1.cast(labels['y'], dtype=tf.int32)
    loss = tf.compat.v1.losses.sparse_softmax_cross_entropy(labels=labels, logits=logits, weights=10)

    if (mode == tf.compat.v1.estimator.ModeKeys.TRAIN):
        optimizer = tf.compat.v1.train.AdamOptimizer(learning_rate=params.get("learning_rate"), epsilon=params.get("epsilon"))
        train_op = optimizer.minimize(loss=loss, global_step=tf.compat.v1.train.get_global_step())
        return tf.compat.v1.estimator.EstimatorSpec(mode=mode, loss=loss, train_op=train_op)

# EVAL==================================================

    accuracy = tf.compat.v1.metrics.accuracy(labels=labels, predictions=predicted_classes, name='acc_op')
    return tf.compat.v1.estimator.EstimatorSpec(mode=mode, loss=loss, eval_metric_ops={"accuracy": accuracy})


def classification_vgg224_fn(features, labels, mode, params):

    if (mode == tf.compat.v1.estimator.ModeKeys.TRAIN):
        is_training = True
    else:
        is_training = False

    x = tf.compat.v1.feature_column.input_layer(features, params["feature_columns"])
    x = tf.compat.v1.identity(x, name='input')
    # 数组新的shape属性应该要与原来的配套，如果等于-1的话，那么Numpy会根据剩下的维度计算出数组的另外一个shape属性值
    x = tf.compat.v1.reshape(x, shape=[-1, 224, 224, 1])
    r = x
# ========================================================================
    x = tf.compat.v1.layers.conv2d(x, 64, 3, activation=tf.nn.relu, padding='same')
    x = tf.compat.v1.layers.conv2d(x, 64, 3, activation=tf.nn.relu, padding='same')
    x = tf.compat.v1.layers.max_pooling2d(x, pool_size=2, strides=2)
    m1 = x
    x = tf.compat.v1.layers.conv2d(x, 128, 3, activation=tf.nn.relu, padding='same')
    x = tf.compat.v1.layers.conv2d(x, 128, 3, activation=tf.nn.relu, padding='same')
    x = tf.compat.v1.layers.max_pooling2d(x, pool_size=2, strides=2)
    m2 = x
    x = tf.compat.v1.layers.conv2d(x, 256, 3, activation=tf.nn.relu, padding='same')
    x = tf.compat.v1.layers.conv2d(x, 256, 3, activation=tf.nn.relu, padding='same')
    x = tf.compat.v1.layers.conv2d(x, 256, 3, activation=tf.nn.relu, padding='same')
    x = tf.compat.v1.layers.max_pooling2d(x, pool_size=2, strides=2)
    m3 = x
    x = tf.compat.v1.layers.conv2d(x, 512, 3, activation=tf.nn.relu, padding='same')
    x = tf.compat.v1.layers.conv2d(x, 512, 3, activation=tf.nn.relu, padding='same')
    x = tf.compat.v1.layers.conv2d(x, 512, 3, activation=tf.nn.relu, padding='same')
    x = tf.compat.v1.layers.max_pooling2d(x, pool_size=2, strides=2)
    m4 = x
    x = tf.compat.v1.layers.conv2d(x, 512, 3, activation=tf.nn.relu, padding='same')
    x = tf.compat.v1.layers.conv2d(x, 512, 3, activation=tf.nn.relu, padding='same')
    x = tf.compat.v1.layers.conv2d(x, 512, 3, activation=tf.nn.relu, padding='same')
    x = tf.compat.v1.layers.max_pooling2d(x, pool_size=2, strides=2)
    m5 = x
    # ===================================================================
    rate = tf.compat.v1.Variable(0.5, name='rate')
    x = tf.compat.v1.layers.flatten(x)
    x = tf.compat.v1.layers.dense(x, 512, activation=tf.nn.relu)
    x = tf.compat.v1.layers.dropout(x, rate=rate, training=is_training)
    x = tf.compat.v1.layers.dense(x, 256, activation=tf.nn.relu)
    x = tf.compat.v1.layers.dropout(x, rate=rate, training=is_training)
    logits = tf.compat.v1.layers.dense(x, params["ySize"], activation=tf.nn.softmax)
    tf.compat.v1.identity(logits, name='output')
    predicted_classes = tf.compat.v1.argmax(input=logits, axis=1)
# PREDICT==================================================
    if (mode == tf.compat.v1.estimator.ModeKeys.PREDICT):
        predictions = {'logits': logits, 'r': r, 'm1': m1, 'm2': m2, 'm3': m3, 'm4': m4, 'm5':m5}
        export_outputs = {'predOutput': tf.compat.v1.estimator.export.PredictOutput(predictions)}
        return tf.compat.v1.estimator.EstimatorSpec(mode=mode, predictions=predictions, export_outputs=export_outputs)
# TRAIN==================================================
    labels = tf.compat.v1.cast(labels['y'], dtype=tf.int32)
    loss = tf.compat.v1.losses.sparse_softmax_cross_entropy(labels=labels, logits=logits, weights=10)

    if (mode == tf.compat.v1.estimator.ModeKeys.TRAIN):
        optimizer = tf.compat.v1.train.GradientDescentOptimizer(learning_rate=params.get("learning_rate"))
        train_op = optimizer.minimize(loss=loss, global_step=tf.compat.v1.train.get_global_step())
        return tf.compat.v1.estimator.EstimatorSpec(mode=mode, loss=loss, train_op=train_op)

# EVAL==================================================

    accuracy = tf.compat.v1.metrics.accuracy(labels=labels, predictions=predicted_classes, name='acc_op')
    return tf.compat.v1.estimator.EstimatorSpec(mode=mode, loss=loss, eval_metric_ops={"accuracy": accuracy})



def regression_cnn1d_fn(features, labels, mode, params):

    x = tf.compat.v1.feature_column.input_layer(features, params["feature_columns"])
    x = tf.identity(x, 'input')
    x = tf.reshape(x, shape=[-1, 64, 64, 1])

    x = tf.compat.v1.layers.conv2d(x, 64, [64, 1], activation=tf.nn.relu, padding='valid')
    x = tf.squeeze(x)
    x = tf.reshape(x, shape=[-1, 64, 64])

    x = tf.compat.v1.layers.conv1d(x, 64, 3, activation=tf.nn.relu, padding='same')
    x = tf.compat.v1.layers.conv1d(x, 64, 3, activation=tf.nn.relu, padding='same')
    x = tf.compat.v1.layers.average_pooling1d(x, 2, 2)

    x = tf.compat.v1.layers.conv1d(x, 128, 3, activation=tf.nn.relu, padding='same')
    x = tf.compat.v1.layers.conv1d(x, 128, 3, activation=tf.nn.relu, padding='same')
    x = tf.compat.v1.layers.average_pooling1d(x, 2, 2)

    x = tf.compat.v1.layers.conv1d(x, 256, 3, activation=tf.nn.relu, padding='same')
    x = tf.compat.v1.layers.conv1d(x, 256, 3, activation=tf.nn.relu, padding='same')
    x = tf.compat.v1.layers.conv1d(x, 256, 3, activation=tf.nn.relu, padding='same')
    x = tf.compat.v1.layers.conv1d(x, 256, 3, activation=tf.nn.relu, padding='same')
    x = tf.compat.v1.layers.average_pooling1d(x, 2, 2)

    x = tf.compat.v1.layers.conv1d(x, 512, 3, activation=tf.nn.relu, padding='same')
    x = tf.compat.v1.layers.conv1d(x, 512, 3, activation=tf.nn.relu, padding='same')
    x = tf.compat.v1.layers.conv1d(x, 512, 3, activation=tf.nn.relu, padding='same')
    x = tf.compat.v1.layers.conv1d(x, 512, 3, activation=tf.nn.relu, padding='same')
    x = tf.compat.v1.layers.average_pooling1d(x, 2, 2)

    x = tf.compat.v1.layers.conv1d(x, 512, 3, activation=tf.nn.relu, padding='same')
    x = tf.compat.v1.layers.conv1d(x, 512, 3, activation=tf.nn.relu, padding='same')
    x = tf.compat.v1.layers.conv1d(x, 512, 3, activation=tf.nn.relu, padding='same')
    x = tf.compat.v1.layers.conv1d(x, 512, 3, activation=tf.nn.relu, padding='same')
    x = tf.compat.v1.layers.average_pooling1d(x, 2, 2)

    x = tf.compat.v1.layers.flatten(x)
    #     x = tf.layers.dense(x, 512, activation=tf.nn.relu)
    #     x = tf.layers.dense(x, 512, activation=tf.nn.relu)
    x = tf.compat.v1.layers.dense(x, units=1)
    y = tf.squeeze(x, name='output')

    #===========================================================
    if (mode == tf.compat.v1.estimator.ModeKeys.PREDICT):
        predictions = {'output': y}
        export_outputs = {'predOutput': tf.estimator.export.PredictOutput(predictions)}
        return tf.compat.v1.estimator.EstimatorSpec(mode=mode, predictions=predictions, export_outputs=export_outputs)
    #===========================================================
    #     average_loss = tf.losses.mean_squared_error(labels, y, weights=1)
    average_loss = tf.compat.v1.losses.absolute_difference(labels, y, weights=1)
    if (mode == tf.compat.v1.estimator.ModeKeys.TRAIN):
        optimizer = tf.compat.v1.train.AdamOptimizer(learning_rate=params.get("learning_rate"), epsilon=1e-6)
        train_op = optimizer.minimize(loss=average_loss, global_step=tf.compat.v1.train.get_global_step())
        return tf.compat.v1.estimator.EstimatorSpec(mode=mode, loss=average_loss, train_op=train_op)
    #===========================================================
    std = calc_std(labels, y)
    eval_metrics = {"std": std}
    return tf.compat.v1.estimator.EstimatorSpec(mode=mode, loss=average_loss, eval_metric_ops=eval_metrics)

