from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from tool import common
from train import trainModel
import threading
import os
import numpy as np
import tensorflow as tf
from tensorflow.compat.v1 import keras
from tensorflow.compat.v1.keras import backend as K


def loadM_thread():
    
    def load():

        model = tf.compat.v1.keras.models.load_model('./model/abc.h5')
        model.summary()
        
        input_node_names = [node.op.name for node in model.inputs]
        print('Input nodes names are: %s', str(input_node_names))
        loss,accuracy = model.evaluate(common.dataProcess_tic_type.get_dataset_test(), steps=33)
        print('\ntest loss',loss)
        print('accuracy',accuracy)
        predictions = model.predict(common.dataProcess_tic_type.get_dataset_test(), steps=33)
        print(predictions)
        print('over predict')
        
        legacy_init_op = tf.group(tf.compat.v1.tables_initializer())
        with K.get_session() as sess:

            export_path = './model/14'
            builder = tf.compat.v1.saved_model.builder.SavedModelBuilder(export_path)
            
            signature_inputs = {
                'main_input': tf.compat.v1.saved_model.utils.build_tensor_info(model.input[0]),
                'feature_input': tf.compat.v1.saved_model.utils.build_tensor_info(model.input[1]),
            }
            
            signature_outputs = {
                tf.compat.v1.saved_model.signature_constants.CLASSIFY_OUTPUT_CLASSES: tf.compat.v1.saved_model.utils.build_tensor_info(model.output)
            }
            
            classification_signature_def = tf.compat.v1.saved_model.signature_def_utils.build_signature_def(
                inputs=signature_inputs,
                outputs=signature_outputs,
                method_name=tf.compat.v1.saved_model.signature_constants.CLASSIFY_METHOD_NAME)
            
            builder.add_meta_graph_and_variables(
                sess,
                [tf.compat.v1.saved_model.tag_constants.SERVING],
                signature_def_map={
                    'predict_webshell_php': classification_signature_def
                },
                legacy_init_op=legacy_init_op
            )
            
            builder.save()
        
    
    th = threading.Thread(target=load)
    th.start()